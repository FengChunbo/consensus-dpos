module chainmaker.org/chainmaker/consensus-dpos/v2

go 1.15

require (
	chainmaker.org/chainmaker/consensus-utils/v2 v2.2.0
	chainmaker.org/chainmaker/logger/v2 v2.2.0
	chainmaker.org/chainmaker/pb-go/v2 v2.2.0
	chainmaker.org/chainmaker/protocol/v2 v2.2.0
	chainmaker.org/chainmaker/utils/v2 v2.2.0
	chainmaker.org/chainmaker/vm-native/v2 v2.2.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/stretchr/testify v1.7.0
	github.com/syndtr/goleveldb v1.0.1-0.20190625010220-02440ea7a285
)
